<?php

use \yii\helpers\Url;
use kartik\tree\TreeView;
use karolprj8\menumanagement\models\Menus;
 

echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => Menus::find()->addOrderBy('root, lft'), 
    'headingOptions' => ['label' => 'Menus'],
    'fontAwesome' => false,     // optional
    'isAdmin' => false,         // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => true,       // defaults to true
    'cacheSettings' => [        
        'enableCache' => false   // defaults to true
    ],
	'iconEditSettings'=>['show'=>'none'],
	'nodeAddlViews' => [
			\kartik\tree\Module::VIEW_PART_2 => '@menumanagement/views/menus/_treePart2'
		]
]);

?>