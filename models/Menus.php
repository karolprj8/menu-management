<?php

namespace karolprj8\menumanagement\models;
 
use Yii;
 
class Menus extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menus';
    }    
    
    /**
     * Override isDisabled method if you need as shown in the  
     * example below. You can override similarly other methods
     * like isActive, isMovable etc.
     */
    public function isDisabled()
    {
        //if (Yii::$app->user->username !== 'admin') {
          //  return true;
        //}
        return parent::isDisabled();
    }
	
	public function rules()
    {
		$rules = parent::rules();
		$rules[] = ['node_url', 'string', 'length' => [0, 212]];
        return $rules;
    }
	
	public function attributeLabels()
    {
        return [
            'name' => Yii::t('menu-management', 'Label'),
            'icon_type' => Yii::t('menu-management', 'Icon Type'),
            'icon' => Yii::t('menu-management', 'Icon'),
            'node_url' => Yii::t('menu-management', 'Module/Controller/Action path or URL'),
        ];
    }
	
	public static function getAsArray($root = 0)
	{
		if ($root == 0) $items = self::find()->where('lvl > 0')->addOrderBy('root, lft')->asArray()->all();
		else  $items = self::find()->where('root = :root AND lvl > 0 ', ['root'=>$root])->addOrderBy('root, lft')->asArray()->all();
		
		$menu = array();
		$z = 0;
		
		if (count($items) > 0)
		{
			$menu[] = array('label'=>$items[0]['name'], 'url'=>[ $items[0]['node_url'] ]);
			for($k=1; $k<count($items); $k++)
			{
				if ($items[$k]['lvl'] > 1)
				{
					if (!array_key_exists('items', $menu[count($menu)-1])) $menu[count($menu)-1]['items'] = array();
					$lvl = array();
					$lvl[1] = $menu[count($menu)-1]['items'];
					for ($l=2; $l<=$items[$k]['lvl']; $l++)
					{
						if ($l == $items[$k]['lvl']) {
							$lvl[$l-1][] = array('label'=>$items[$k]['name'], 'url'=>[ $items[$k]['node_url'] ]);
						} else {
							if (!array_key_exists('items', $lvl[$l-1][count($lvl[$l-1])-1])) $lvl[$l-1][count($lvl[$l-1])-1]['items'] = array();
							$lvl[$l] = $lvl[$l-1][count($lvl[$l-1])-1]['items'];
						}
					}
					for ($l=count($lvl)-1; $l>0; $l--)
					{
						$lvl[$l][count($lvl[$l])-1]['items'] = $lvl[$l+1];
					}
					$menu[count($menu)-1]['items'] = $lvl[1];
				} else {
					$menu[] = array('label'=>$items[$k]['name'], 'url'=>[ $items[$k]['node_url'] ]);;
				}
			}
		}
		
		return $menu;
	}
}

?>